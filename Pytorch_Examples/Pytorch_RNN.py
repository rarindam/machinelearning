#https://github.com/gabrielloye/RNN-walkthrough/blob/master/main.ipynb
import torch
from torch import nn
import nltk
import numpy as np
import multiprocessing

def file_to_list(file_name):
    with open(file_name, 'r') as in_file:
        text = in_file.read()
        sentences = nltk.sent_tokenize(text)
        for i in range(len(sentences)):
            sentences[i] = sentences[i].strip().lower().replace("\n", "").replace("  ", "")

    return sentences


text = file_to_list('./Data/SherlockHolmes.txt')
#text = ['hey how are you','good i am fine','have a nice day']


chars = set(''.join(text))

int2char = dict(enumerate(chars))

char2int = {char: ind for ind, char in int2char.items()}

#Pad input sentences to ensure all of them are same length

maxlen = len(max(text, key=len))
# Now pad all the sentences with blanks

for i in range(len(text)):
    while len(text[i]) < maxlen:
        text[i] += ' '

#Create list that will hold our input and target sequences

input_seq = []
target_seq = []

for i in range(len(text)):
    #Remove last character from input sequence
    input_seq.append(text[i][:-1])

    #Remove 1st character for target sequence

    target_seq.append(text[i][1:])
    print (f"Input Sequence:{input_seq[i]},\nTarget Sequence:{target_seq[i]}")

# Convert input and output sequences to integers
for i in range(len(text)):
    input_seq[i] = [char2int[character] for character in input_seq[i]]
    target_seq[i] = [char2int[character] for character in target_seq[i]]

#Before getting the one hot vector ready, define 3 key variables:

dict_size = len(char2int)
seq_len = maxlen - 1
batch_size = len(text)

# Setup thread count to use all online CPU Cores
torch.set_num_threads(multiprocessing.cpu_count())

def one_hot_encode(sequence, dict_size, seq_len, batch_size):
    # Create a multi-dimensional array of zeros with the desired output shape
    features = np.zeros((batch_size, seq_len, dict_size), dtype=np.float32)

    #Replace 0 with 1 at the relevant character index
    for i in range(batch_size):
        for u in range(seq_len):
            features[i, u, sequence[i][u]] = 1
    return features

input_seq = one_hot_encode(input_seq, dict_size, seq_len, batch_size)
print (f"Input Shape: {input_seq.shape}--> (Batch Size, Sequence Length, One-Hot Encoding Size)")

# Now move the data from Numpy array to PyTorch Tensor

input_seq = torch.from_numpy(input_seq)
target_seq = torch.Tensor(target_seq)

# Check whether running on GPU
is_cuda = torch.cuda.is_available()

if is_cuda:
    device = torch.device("cuda")
    print ("GPU is available")
else:
    device = torch.device("cpu")
    print("GPU is not available, using CPU")

# Using a 1 layer RNN, followed by a fully connected layer
# The fully connected layer is used to convert RNN output to desired shape

class Model(nn.Module):
    def __init__(self, input_size, output_size, hidden_dim, n_layers):
        super (Model, self).__init__()
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers

        # Define the layers
        self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=True)
        #Fully connected layer (input shape, output_shape
        self.fc = nn.Linear(hidden_dim, output_size)

    def forward(self, x):
        batch_size = x.size(0)

        # Initialize hidden state for input
        hidden = self.init_hidden(batch_size)

        # Passing in the input and hidden state into the model, to get output

        out, hidden = self.rnn(x, hidden)

        # Reshape the outputs to fully connected layer

        out = out.contiguous().view(-1, self.hidden_dim)
        out = self.fc(out)

        return out, hidden

    def init_hidden(self, batch_size):
        # Create first hidden state with all zeros to be used in forward pass
        hidden = torch.zeros(self.n_layers, batch_size, self.hidden_dim).to(device)
        return hidden

# Now create Model and define hyperparameters

model = Model(input_size=dict_size, output_size=dict_size, hidden_dim=12, n_layers=1)
#There should be a CUDA call here
model = model.to(device)

#Define Hyperparameters
n_epochs = 100
lr = 0.01

# Define Loss function, Optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr = lr)

#Training Run
input_seq = input_seq.to(device)
for epoch in range (1, n_epochs + 1):
    optimizer.zero_grad()
    output, hidden = model(input_seq)
    output = output.to(device)
    target_seq = target_seq.to(device)

    #Calculate loss
    loss = criterion(output, target_seq.view(-1).long())

    #Backpropagate and calculate gradient
    loss.backward()

    #Update the weights
    optimizer.step()

    if epoch%10 == 0:
        print (f"Epoch:{epoch}/{n_epochs}")
        print (f"Loss:{loss.item():.4f}")

def predict (model, character):
    # Convert output to character
    character = np.array([[char2int[c] for c in character]])
    character = one_hot_encode(character, dict_size, character.shape[1], 1)
    character = torch.from_numpy(character)
    character = character.to(device)

    out, hidden = model(character)
    probability = nn.functional.softmax(out[-1], dim=0).data

    # Take the class with the highest probability score from output
    char_ind = torch.max(probability, dim=0)[1].item()

    return int2char[char_ind], hidden

def sample(model, out_len, start = 'hey'):
    model.eval()
    start = start.lower()
    #Run through the starting characters
    chars = [ch for ch in start]
    size = out_len - len(chars)
    for ii in range(size):
        char, h = predict (model, chars)
        chars.append(char)

    return ''.join(chars)

print (sample(model, 15, 'how'))

